(put 'narrow-to-region 'disabled nil)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(safe-local-variable-values
   '((eval add-hook 'before-save-hook
      (lambda nil
        (org-babel-ref-resolve "list"))
      nil t)
     (eval save-excursion
      (org-babel-goto-named-src-block "breakdown")
      (org-babel-execute-src-block)
      (if
          (org-redisplay-inline-images)
          (message "Images Redisplayed"))))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
