(setq tab-always-indent t)
(setq org-roam-directory "~/org/")

(setq doom-theme 'doom-oceanic-next)
(setq display-line-numbers-type t)
(setq auto-save-default nil)
(setq doom-modeline-major-mode-icon t)

(remove-hook '+doom-dashboard-functions #'doom-dashboard-widget-shortmenu)
(remove-hook '+doom-dashboard-functions #'doom-dashboard-widget-footer)
(setq fancy-splash-image (concat doom-private-dir "emacs.png"))

(defun display-new-buffer (buffer force-other-window)
  "If BUFFER is visible, select it.
If it's not visible and there's only one window, split the
current window and select BUFFER in the new window. If the
current window (before the split) is more than 100 columns wide,
split horizontally(left/right), else split vertically(up/down).
If the current buffer contains more than one window, select
BUFFER in the least recently used window.
This function returns the window which holds BUFFER.
FORCE-OTHER-WINDOW is ignored."
  (or (get-buffer-window buffer)
    (if (one-window-p)
        (let ((new-win
               (if (> (window-width) 100)
                   (split-window-horizontally)
                 (split-window-vertically))))
          (set-window-buffer new-win buffer)
          new-win)
      (let ((new-win (get-lru-window)))
        (set-window-buffer new-win buffer)
        new-win))))
;; use display-buffer-alist instead of display-buffer-function if the following line won't work
(setq display-buffer-function 'display-new-buffer)

(setq user-full-name "Judah Sotomayor"
      user-mail-address "")

(map! :desc "Switch to normal mode" "M-c" #'evil-normal-state)

(use-package! evil-colemak-basics
  :init (setq evil-colemak-basics-layout-mod 'mod-dh)
 )

(setq org-pretty-entities 0)
(setq evil-colemak-basics-layout-mod 'mod-dh)

(map!
 :desc "Switch key layouts" "M-C-K" #'global-evil-colemak-basics-mode)

(defun bash ()
  (interactive)
  (term "/bin/bash"))
(map!
      :desc "Start a bash shell"
      "<f6>" #'bash)

(add-hook 'term-mode-hook 'hide-mode-line-mode)

(use-package! sage-shell-mode
  :defer)

;; Ob-sagemath supports only evaluating with a session.
(setq org-babel-default-header-args:sage '((:session . t)
                                           (:results . "output")))

;; C-c c for asynchronous evaluating (only for SageMath code blocks).
(with-eval-after-load "org"
  (define-key org-mode-map (kbd "C-c c") 'ob-sagemath-execute-async))

(map! :leader
      :desc "The Emacs Calculator"
      "C" #'calc)

(defun fava ()
  (interactive)
  (async-shell-command "flatpak run org.gnome.gitlab.johannesjh.favagtk"))
(map! :leader
 :desc "Start favagtk"
 "r b" #'fava)

(use-package! ob-mermaid)

(use-package! ob-svgbob)
(setq org-svgbob-executable "svgbob_cli")

(setq org-babel-svgbob--parameters
  '(:background transparent
    ))

(setq fortune-file "~/org/fortunes")
(map! :leader (:prefix-map("F" . "fortune")

      :desc "Display a random verse in the minibuffer"
      "r" #'fortune-message

      :desc "Display a random verse"
      "R" #'fortune-message

      :desc "Add new verse in the prompt"
      "a r" #'fortune-add-fortune

      :desc "Add selected region to list of verses"
      "a R" #'fortune-from-region

        ))

(setq doom-quit-messages
      '(
    "Please don't leave, there's more demons to toast!"
    "Let's beat it -- This is turning into a bloodbath!"
    "I wouldn't leave if I were you. DOS is much worse."
    "Don't leave yet -- There's a daemon around that corner!"
    "Ya know, next time you come in here I'm gonna toast ya."
    "Go ahead and leave. See if I care."
    "Are you sure you want to quit this great editor?"
    ;; from Portal
    "You can't fire me, I quit!"
    "I don't know what you think you are doing, but I don't like it. I want you to stop."
    "This isn't brave. It's murder. What did I ever do to you?"
    "I'm the man who's going to burn your house down! With the lemons!"
    "Okay, look. We've both said a lot of things you're going to regret..."
    ;; Custom
    "(setq nothing t everything 'permitted)"
    "Emacs will remember that."
    "Emacs, Emacs never changes."
    "Hey! Hey, M-x listen!"
    "It's not like I'll miss you or anything"
    "Wake up, Mr. Stallman. Wake up and smell the ashes."
    "You are *not* prepared!"
    "Please don't go. The drones need you. They look up to you."
    ))

(defun current-date () (interactive)
    (shell-command-to-string " bash -c 'echo -n $(date +%Y-%m-%d)'"))
 (defun insert-current-date () (interactive)
    (insert (current-date)))

(map! :leader
      :desc "Insert the current date into the buffer"
      "i d" #'insert-current-date)

(use-package! age
  :ensure t
  :demand t
  :custom
  (age-program "age")
  (age-default-identity "~/.age/personal")
  (age-default-recipient "~/.age/personal.pub")
  :config
  (age-file-enable))

(setq org-confirm-babel-evaluate t)
(setq org-link-shell-confirm-function t)
(setq org-link-elisp-confirm-function t)
(setq enable-local-variables "ask")
(setq enable-local-eval 'maybe)

(after! org-roam
(defun vulpea-project-p ()
  "Return non-nil if current buffer has any todo entry.
TODO entries marked as done are ignored, meaning the this
function returns nil if current buffer contains only completed
tasks."
  (seq-find                                 ; (3)
   (lambda (type)
     (eq type 'todo))
   (org-element-map                         ; (2)
       (org-element-parse-buffer 'headline) ; (1)
       'headline
     (lambda (h)
       (org-element-property :todo-type h)))))

(defun vulpea-project-update-tag ()
    "Update PROJECT tag in the current buffer."
    (when (and (not (active-minibuffer-window))
               (vulpea-buffer-p))
      (save-excursion
        (goto-char (point-min))
        (let* ((tags (vulpea-buffer-tags-get))
               (original-tags tags))
          (if (vulpea-project-p)
              (setq tags (cons "hastodos" tags))
            (setq tags (remove "hastodos" tags)))

          ;; cleanup duplicates
          (setq tags (seq-uniq tags))

          ;; update tags if changed
          (when (or (seq-difference tags original-tags)
                    (seq-difference original-tags tags))
            (apply #'vulpea-buffer-tags-set tags))))))

(defun vulpea-buffer-p ()
  "Return non-nil if the currently visited buffer is a note."
  (and buffer-file-name
       (string-prefix-p
        (expand-file-name (file-name-as-directory org-roam-directory))
        (file-name-directory buffer-file-name))))

(defun vulpea-project-files ()
    "Return a list of note files containing 'hastodos' tag." ;
    (seq-uniq
     (seq-map
      #'car
      (org-roam-db-query
       [:select [nodes:file]
        :from tags
        :left-join nodes
        :on (= tags:node-id nodes:id)
        :where (like tag (quote "%\"hastodos\"%"))]))))

(defun vulpea-agenda-files-update (&rest _)
  "Update the value of `org-agenda-files'."
  (setq org-agenda-files (vulpea-project-files)))

(add-hook 'find-file-hook #'vulpea-project-update-tag)
(add-hook 'before-save-hook #'vulpea-project-update-tag)

(advice-add 'org-agenda :before #'vulpea-agenda-files-update)
(advice-add 'org-todo-list :before #'vulpea-agenda-files-update)

;; functions borrowed from `vulpea' library
;; https://github.com/d12frosted/vulpea/blob/6a735c34f1f64e1f70da77989e9ce8da7864e5ff/vulpea-buffer.el

(defun vulpea-buffer-tags-get ()
  "Return filetags value in current buffer."
  (vulpea-buffer-prop-get-list "filetags" "[ :]"))

(defun vulpea-buffer-tags-set (&rest tags)
  "Set TAGS in current buffer.
If filetags value is already set, replace it."
  (if tags
      (vulpea-buffer-prop-set
       "filetags" (concat ":" (string-join tags ":") ":"))
    (vulpea-buffer-prop-remove "filetags")))

(defun vulpea-buffer-tags-add (tag)
  "Add a TAG to filetags in current buffer."
  (let* ((tags (vulpea-buffer-tags-get))
         (tags (append tags (list tag))))
    (apply #'vulpea-buffer-tags-set tags)))

(defun vulpea-buffer-tags-remove (tag)
  "Remove a TAG from filetags in current buffer."
  (let* ((tags (vulpea-buffer-tags-get))
         (tags (delete tag tags)))
    (apply #'vulpea-buffer-tags-set tags)))

(defun vulpea-buffer-prop-set (name value)
  "Set a file property called NAME to VALUE in buffer file.
If the property is already set, replace its value."
  (setq name (downcase name))
  (org-with-point-at 1
    (let ((case-fold-search t))
      (if (re-search-forward (concat "^#\\+" name ":\\(.*\\)")
                             (point-max) t)
          (replace-match (concat "#+" name ": " value) 'fixedcase)
        (while (and (not (eobp))
                    (looking-at "^[#:]"))
          (if (save-excursion (end-of-line) (eobp))
              (progn
                (end-of-line)
                (insert "\n"))
            (forward-line)
            (beginning-of-line)))
        (insert "#+" name ": " value "\n")))))

(defun vulpea-buffer-prop-set-list (name values &optional separators)
  "Set a file property called NAME to VALUES in current buffer.
VALUES are quoted and combined into single string using
`combine-and-quote-strings'.
If SEPARATORS is non-nil, it should be a regular expression
matching text that separates, but is not part of, the substrings.
If nil it defaults to `split-string-default-separators', normally
\"[ \f\t\n\r\v]+\", and OMIT-NULLS is forced to t.
If the property is already set, replace its value."
  (vulpea-buffer-prop-set
   name (combine-and-quote-strings values separators)))

(defun vulpea-buffer-prop-get (name)
  "Get a buffer property called NAME as a string."
  (org-with-point-at 1
    (when (re-search-forward (concat "^#\\+" name ": \\(.*\\)")
                             (point-max) t)
      (buffer-substring-no-properties
       (match-beginning 1)
       (match-end 1)))))

(defun vulpea-buffer-prop-get-list (name &optional separators)
  "Get a buffer property NAME as a list using SEPARATORS.
If SEPARATORS is non-nil, it should be a regular expression
matching text that separates, but is not part of, the substrings.
If nil it defaults to `split-string-default-separators', normally
\"[ \f\t\n\r\v]+\", and OMIT-NULLS is forced to t."
  (let ((value (vulpea-buffer-prop-get name)))
    (when (and value (not (string-empty-p value)))
      (split-string-and-unquote value separators))))

(defun vulpea-buffer-prop-remove (name)
  "Remove a buffer property called NAME."
  (org-with-point-at 1
    (when (re-search-forward (concat "\\(^#\\+" name ":.*\n?\\)")
                             (point-max) t)
      (replace-match ""))))

    (setq org-agenda-files (vulpea-project-files)))

(setq org-log-done 'time)
(after! org-mode
    (setq org-log-done 'time)
    (setq org-archive-location "~/org/archive.org")
    (add-to-list 'org-tags-exclude-from-inheritance 'hastodos)
    (setq org-hide-emphasis-markers nil))

(setq org-directory "~/org/")
(setq org-roam-directory org-directory)
(setq org-archive-location (concat org-directory "/archive.org::"))
(add-to-list 'org-modules 'org-habit-plus t)

(setq org-todo-keywords
      '((sequence "PROJ(p)" "|" "COMPLETE")
        (type "TICKLE")
        (type "DELE")
        (sequence   "WAIT" "TODO(t)"  "|"  "DONE")
        (sequence "DELEGATED" "VERIFY" "|" "DONE")
        (sequence "BUY(b)" "|" "BOUGHT")
        ))
(setq org-todo-keyword-faces
      '(("TODO" . "salmon1")
        ("START" . "spring green" )
        ("DELE" . "gold")
        ("TICKLE" . "magenta")
        ("WAIT" . "gold")
        ("PROJ" . "deep sky blue")))

(setq org-modern-todo-faces
      '(("START" :background "spring green" :foreground "black")
        ("DELE" :background "gold" :foreground "black")
        ("WAIT" :background "gold" :foreground "black")
        ("PROJ" :background "deep sky blue" :foreground "black")
        ("TICKLE" :background "magenta" :foreground "black")))

(setq org-modern-priority-faces
      '((?A :background "salmon1" :foreground "black")
        (?B :background "gold" :foreground "black")
        (?C :background "spring green" :foreground "black")))

(setq org-startup-with-inline-images t)

;; Show images after evaluating code blocks.
(add-hook 'org-babel-after-execute-hook 'org-display-inline-images)

(setq-default org-html-with-latex `dvisvgm)
 (setq browse-url-browser-function 'eww)
(setq org-latex-pdf-process
       '("latexmk -pdflatex='pdflatex -interaction nonstopmode' -pdf -bibtex -f %f"))



 (unless (boundp 'org-latex-classes)
   (setq org-latex-classes nil))

 (add-to-list 'org-latex-classes
              '("ethz"
                "\\documentclass[a4paper,11pt,titlepage]{memoir}
 \\usepackage[utf8]{inputenc}
 \\usepackage[T1]{fontenc}
 \\usepackage{fixltx2e}
 \\usepackage{graphicx}
 \\usepackage{longtable}
 \\usepackage{float}
 \\usepackage{wrapfig}
 \\usepackage{rotating}
 \\usepackage[normalem]{ulem}
 \\usepackage{amsmath}
 \\usepackage{textcomp}
 \\usepackage{marvosym}
 \\usepackage{wasysym}
 \\usepackage{amssymb}
 \\usepackage{hyperref}
 \\usepackage{mathpazo}
 \\usepackage{color}
 \\usepackage{enumerate}
 \\definecolor{bg}{rgb}{0.95,0.95,0.95}
 \\tolerance=1000
       [NO-DEFAULT-PACKAGES]
       [PACKAGES]
       [EXTRA]
 \\linespread{1.1}
 \\hypersetup{pdfborder=0 0 0}"
                ("\\chapter{%s}" . "\\chapter*{%s}")
                ("\\section{%s}" . "\\section*{%s}")
                ("\\subsection{%s}" . "\\subsection*{%s}")
                ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                ("\\paragraph{%s}" . "\\paragraph*{%s}")
                ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))


 (add-to-list 'org-latex-classes
              '("article"
                "\\documentclass[11pt,a4paper]{article}
 \\usepackage[utf8]{inputenc}
 \\usepackage[T1]{fontenc}
 \\usepackage{fixltx2e}
 \\usepackage{graphicx}
 \\usepackage{longtable}
 \\usepackage{float}
 \\usepackage{wrapfig}
 \\usepackage{rotating}
 \\usepackage[normalem]{ulem}
 \\usepackage{amsmath}
 \\usepackage{textcomp}
 \\usepackage{marvosym}
 \\usepackage{wasysym}
 \\usepackage{amssymb}
 \\usepackage{hyperref}
 \\usepackage{mathpazo}
 \\usepackage{color}
 \\usepackage{enumerate}
 \\definecolor{bg}{rgb}{0.95,0.95,0.95}
 \\tolerance=1000
       [NO-DEFAULT-PACKAGES]
       [PACKAGES]
       [EXTRA]
 \\linespread{1.1}
 \\hypersetup{pdfborder=0 0 0}"
                ("\\section{%s}" . "\\section*{%s}")
                ("\\subsection{%s}" . "\\subsection*{%s}")
                ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                ("\\paragraph{%s}" . "\\paragraph*{%s}")))


 (add-to-list 'org-latex-classes '("ebook"
                                   "\\documentclass[11pt, oneside]{memoir}
 \\setstocksize{9in}{6in}
 \\settrimmedsize{\\stockheight}{\\stockwidth}{*}
 \\setlrmarginsandblock{2cm}{2cm}{*} % Left and right margin
 \\setulmarginsandblock{2cm}{2cm}{*} % Upper and lower margin
 \\checkandfixthelayout
 % Much more laTeX code omitted
 "
                                   ("\\chapter{%s}" . "\\chapter*{%s}")
                                   ("\\section{%s}" . "\\section*{%s}")
                                   ("\\subsection{%s}" . "\\subsection*{%s}")))

(defun +org-refresh-latex-images-previews-h ()
  (dolist (buffer (doom-buffers-in-mode 'org-mode (buffer-list)))
    (with-current-buffer buffer
      (+org--toggle-inline-images-in-subtree (point-min) (point-max) 'refresh)
      (unless (eq org-latex-preview-default-process 'dvisvgm)
        (org-clear-latex-preview (point-min) (point-max))
        (org--latex-preview-region (point-min) (point-max))))))

(add-hook 'doom-load-theme-hook #'+org-refresh-latex-images-previews-h)

(defun eww-open-this-file ()
       (set 'directory (file-name-directory buffer-file-name))
       (set 'filename (file-name-sans-extension buffer-file-name))
       (set 'extension ".html")
       (set 'filename (format "%s%s" filename extension))
       (eww-open-file filename)
       (evil-window-left)
)

(defun org-export-and-open-eww ()
    "Export current file to html and open in eww"
    (interactive)

    (org-html-export-to-html)
    (if (equal (file-name-extension buffer-file-name) "org")
         (eww-open-this-file)
      (message "Failed")
    )
)

(map! :leader
        :desc "Export to html and diplay with eww"
        "r E" #'org-export-and-open-eww)

(use-package! org-super-agenda
  :hook (org-agenda-mode . org-super-agenda-mode ))
(setq org-super-agenda-groups
       '(
         (:name "Upcoming"
                :deadline future
                :scheduled future
                )


         (:name "Maintenance"
                :tag "chore"
                :tag "maintenance"
                :tag "system")
         (:name "Next"; Optionally specify section name
                :tag "next"
                :todo "NEXT"
                )  ; Items that have this TODO keyword

         (:name "Important"
                ;; Single arguments given alone
                :tag "bills"
                :priority "A")
         ;; Set order of multiple groups at once
         (:name "Drills"
                :tag "drill"
                :order 98)
         ;; Groups supply their own section names when none are given
         (:todo "WAIT" :order 8)  ; Set order of this section
         (:todo ("SOMEDAY" "TO-READ" "CHECK" "TO-WATCH" "WATCHING")
                ;; Show this group at the end of the agenda (since it has the
                ;; highest number). If you specified this group last, items
                ;; with these todo keywords that e.g. have priority A would be
                ;; displayed in that group instead, because items are grouped
                ;; out in the order the groups are listed.
                :order 9)

         (:priority<= "B" :order 10)
                      ;; Show this section after "Today" and "Important", because
                      ;; their order is unspecified, defaulting to 0. Sections
                      ;; are displayed lowest-number-first.
         ;; After the last group, the agenda will display items that didn't
         ;; match any of these groups, with the default order position of 99
         ))

(use-package! org-ql)
(map! :leader (:prefix-map ("r" . "roam"))
      :desc "Org-ql view"
      "v" #'org-ql-view   )

(defun org-ql-agenda-daily ()
(interactive)

(org-ql-search
  (org-agenda-files)
  '(and (not (done)) (or (habit) (deadline auto) (scheduled :to today)  (and (not (scheduled)) (todo))(ts-active :on today) ))
  :super-groups org-super-agenda-groups))


(map! :leader (:prefix ("r" . "roam")
                :desc "Display daily agenda"
                "a" #'org-ql-agenda-daily
                ))

(use-package! org-roam-ql)

(use-package! org-modern
  :config

    ;; Minimal UI
    (package-initialize)
    (menu-bar-mode -1)
    (tool-bar-mode -1)
    (scroll-bar-mode -1)
    ;(modus-themes-load-operandi)

    ;; Choose some fonts
    ;; (set-face-attribute 'default nil :family "Iosevka")
    ;; (set-face-attribute 'variable-pitch nil :family "Iosevka Aile")
    ;; (set-face-attribute 'org-modern-symbol nil :family "Iosevka")

    ;; Add frame borders and window dividers
    (modify-all-frames-parameters
    '((right-divider-width . 0)
    (internal-border-width . 0)))
    (dolist (face '(window-divider
                    window-divider-first-pixel
                    window-divider-last-pixel))
    (face-spec-reset-face face)
    (set-face-foreground face (face-attribute 'default :background)))
    (set-face-background 'fringe (face-attribute 'default :background))

    (setq
    ;; Edit settings
    org-auto-align-tags nil
    org-tags-column 0
    org-catch-invisible-edits 'show-and-error
    org-special-ctrl-a/e t
    org-insert-heading-respect-content t

    ;; Org styling, hide markup etc.
    org-hide-emphasis-markers t
    org-pretty-entities 0
    org-ellipsis "…"

    ;; Agenda styling
    org-agenda-tags-column 0
    org-agenda-block-separator ?─
    org-agenda-time-grid
    '((daily today require-timed)
    (800 1000 1200 1400 1600 1800 2000)
    " ┄┄┄┄┄ " "┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄")
    org-agenda-current-time-string
    "⭠ now ─────────────────────────────────────────────────")

    (global-org-modern-mode)

                )

(use-package! org-drill
  :defer nil
  )

  (map! :leader
        :desc "Start org-drill"
        "d d" #'org-drill-directory)

  (map! :leader
        :desc "Start org-drill in cram mode"
        "d c" #'org-drill-cram)

(use-package! ob-lilypond)
(setq ly-arrange-mode t)

(use-package! org-auto-tangle
  :defer t
  :hook (org-mode . org-auto-tangle-mode))

(use-package! citar
  :ensure t
  :demand t
  :custom
  (citar-bibliography "~/org/references.bib")
  (org-cite-global-bibliography '("~/org/references.bib"))



  (org-cite-csl-styles-dir
   (expand-file-name "~/Zotero/styles/"))
  (org-cite-export-processors
    '((t . (csl "apa.csl")) ))

  :hook
  (LaTeX-mode . citar-capf-setup)
  (org-mode . citar-capf-setup))
(after! org-mode
  (org-cite-global-bibliography '("~/org/references.bib"))
  )

(add-to-list 'org-latex-classes
    '("student-apa7"
      "\\documentclass[stu]{apa7}"
     ("\\section{%s}" . "\\section*{%s}")
     ("\\subsection{%s}" . "\\subsection*{%s}")
     ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
     ("\\paragraph{%s}" . "\\paragraph*{%s}")
     ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

(add-to-list 'org-latex-classes
    '("student-mla"
      "\\documentclass[stu]{mla}"
     ("\\section{%s}" . "\\section*{%s}")
     ("\\subsection{%s}" . "\\subsection*{%s}")
     ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
     ("\\paragraph{%s}" . "\\paragraph*{%s}")
     ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

       ;; org-latex-compilers = ("pdflatex" "xelatex" "lualatex"), which are the possible values for %latex
(setq org-latex-pdf-process '("LC_ALL=en_US.UTF-8 latexmk -f -pdf -%latex -shell-escape -interaction=nonstopmode -output-directory=%o %f"     ))

(use-package! engrave-faces)
(setq org-latex-listings 'engraved)

(use-package! org-roam
  :after md-roam
  :init (setq org-roam-directory "~/org/")
  :custom
  (org-roam-capture-templates
   '(("d" "default" plain "%?"
      :target (file+head "%<%Y%m%d-%H%M%S>.org"
                         "#+TITLE: ${title}\n #+HTML_HEAD: <link rel=\"stylesheet\" type=\"text/css\" href=\"css/retro.css\" />\n")
      :unnarrowed t)
     ("c" "encrypted" plain "%?"
      :target (file+head "%<%Y%m%d-%H%M%S>.sec.org.age"
                         "#+TITLE: ${title}\n #+FILETAGS: :secure:noexport:\n #+HTML_HEAD: <link rel=\"stylesheet\" type=\"text/css\" href=\"css/retro.css\" />\n\n\n* No Export Below This Line")
      :unnarrowed t))))

(after! org-roam (map! :leader (:prefix ("r" . "roam")

                :desc "Search for a node in org-roam files"
                "f" #'org-roam-node-find

                :desc "Insert the link to a node from org-roam"
                "i" #'org-roam-node-insert

                :desc "Rescan org-roam files and add new nodes to database"
                "s" #'org-roam-db-sync

                :desc "Jump to a random node"
                "r" #'org-roam-node-random

                :desc "Capture inside an existing or new node"
                "c" #'org-roam-capture

                :desc "Go to or create a daily note"
                "d" #'org-roam-dailies-goto-today

                :desc "Seek a daily note"
                "D" #'org-roam-dailies-find-date

                :desc "Extract a subtree to a new file"
                "e" #'org-roam-extract-subtree))

            (map!
                :desc "Alternative keybind to insert a roam link while in insert mode"
                :i "M-[" #'org-roam-node-insert))

(use-package! websocket
    :after org-roam)

(use-package! org-roam-ui
    :after org-roam ;; or :after org
;;         normally we'd recommend hooking orui after org-roam, but since org-roam does not have
;;         a hookable mode anymore, you're advised to pick something yourself
;;         if you don't care about startup time, use
;;  :hook (after-init . org-roam-ui-mode)
    :config
    (setq org-roam-ui-sync-theme t
          org-roam-ui-follow t
          org-roam-ui-update-on-save t
          org-roam-ui-open-on-start t))

(after! org-roam-ui (map! :leader (:prefix ("r" . "roam") (:prefix ("u" . "roam-ui")

                :desc "Focus the current node in org-roam-ui view"
                "f" #'org-roam-ui-node-zoom

                :desc "Focus the current node's local graph in org-roam-ui view"
                "l" #'org-roam-ui-node-local

                :desc "Begin org-roam ui mode"
                "u" #'org-roam-ui-mode

                ))))

(after! org-roam
(setq org-roam-dailies-capture-templates
    '(("d" "default" plain
       (file '(concat user-emacs-directory "templates/journal.sec.org.age"))
       :if-new (file+head "%<%Y-%m-%d>.sec.org.age"
                          "#+TITLE: %<%Y-%m-%d>\n#+FILETAGS: :noexport:secure:\n\n")
        :unnarrowed t
       ))))

(require 'cl-lib)

(defun org-global-props-key-re (key)
  "Construct a regular expression matching key and an optional plus and eating the spaces behind.
Test for existence of the plus: (match-beginning 1)"
  (concat "^" (regexp-quote key) "\\(\\+\\)?[[:space:]]+"))

(defun org-global-props (&optional buffer)
  "Get the plists of global org properties of current buffer."
  (with-current-buffer (or buffer (current-buffer))
    (org-element-map (org-element-parse-buffer) 'keyword (lambda (el) (when (string-equal (org-element-property :key el) "PROPERTY") (nth 1 el))))))

(defun org-global-prop-value (key)
  "Get global org property KEY of current buffer.
Adding up values for one key is supported."
  (let ((key-re (org-global-props-key-re key))
    (props (org-global-props))
    ret)
    (cl-loop with val for prop in props
         when (string-match key-re (setq val (plist-get prop :value))) do
         (setq
          val (substring val (match-end 0))
          ret (if (match-beginning 1)
              (concat ret " " val)
            val)))
    ret))

(use-package! md-roam
  :custom
    (setq md-roam-file-extension "md") ; default "md". Specify an extension such as "markdown"
  )
(md-roam-mode 1) ; md-roam-mode must be active before org-roam-db-sync

(after! company-mode
  (setq company-idle-delay nil))
(setq company-idle-delay nil)
